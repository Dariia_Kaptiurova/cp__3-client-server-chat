package dao;

import model.Chat;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateSessionFactoryUtil;

import java.util.ArrayList;
import java.util.List;

public class ChatDao {

    public void update(Chat chat) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(chat);
        tx1.commit();
        session.close();
    }

    public void saveChat(Chat chat) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(chat);
        tx1.commit();
        session.close();
    }

    public void delete(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.createQuery("DELETE Chat WHERE id_chat = :id")
                .setParameter("id", id)
                .executeUpdate();
        tx1.commit();
        session.close();
    }

    public List<Chat> readChatByUser(String login) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Chat> list =
                (ArrayList<Chat>) session
                        .createQuery("SELECT a FROM Chat a JOIN FETCH a.users b where b.login=:login")
                        .setParameter("login",login)
                        .list();
//        String json =
//                new Gson().toJson((ArrayList<Chat>) session
//                        .createQuery("SELECT a FROM Chat a JOIN FETCH a.users b where b.login=:login")
//                        .setParameter("login",login)
//                        .list());
//        System.out.println(new Gson().toJson(list));
        session.close();
        return list;
    }

    public Chat getById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession()
                .get(Chat.class, id);
    }


}
