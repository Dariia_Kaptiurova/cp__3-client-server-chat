package dto;

public class LoginMessage {
    String login;
    String password;

    public LoginMessage(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
